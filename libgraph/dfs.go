package libgraph

func (g *Graph) DFS(start int, f func(*GraphNode) int) {
	stack := Stack{}
	stack.New()

	s_node := g.GetNode(start)
	stack.Push(s_node)
	flags := make(map[*GraphNode]bool)

	for {
		if stack.IsEmpty() {
			break
		}

		node := stack.Pop()
		flags[node] = true
		adj := g.Neighbours(*node)

		for i := (len(adj) - 1); i >= 0; i-- {
			j := adj[i]
			if !flags[j] {
				stack.Push(j)
				flags[j] = true
			}
		}

		if f != nil {
			c := f(node)
			if c == -1 {
				break
			}
		}
	}
}
