package libgraph

import "fmt"

type NodeQueue struct {
	nodes []*GraphNode
}

func (q *NodeQueue) New() *NodeQueue {
	q.nodes = []*GraphNode{}
	return q
}

func (q *NodeQueue) Enque(n *GraphNode) {
	q.nodes = append(q.nodes, n)
}

func (q *NodeQueue) Dequeue() *GraphNode {
	item := q.nodes[0]
	q.nodes = q.nodes[1:len(q.nodes)]

	return item
}

func (q *NodeQueue) Isempty() bool {
	return len(q.nodes) == 0
}

func (q *NodeQueue) Size() int {
	return len(q.nodes)
}

func (q *NodeQueue) String() {
	for i := 0; i < len(q.nodes); i++ {
		fmt.Printf("%v\t", q.nodes[i])
	}
	fmt.Printf("\n")
}
