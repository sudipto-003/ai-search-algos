package libgraph

func (g *Graph) BFS(start int, f func(*GraphNode) int) {
	queue := NodeQueue{}
	queue.New()

	s_node := g.GetNode(start)
	queue.Enque(s_node)
	flags := make(map[*GraphNode]bool)

	for {
		if queue.Isempty() {
			break
		}

		node := queue.Dequeue()
		flags[node] = true
		adj := g.Neighbours(*node)

		for i := 0; i < len(adj); i++ {
			j := adj[i]
			if !flags[j] {
				queue.Enque(j)
				flags[j] = true
			}
		}

		if f != nil {
			c := f(node)
			if c == -1 {
				break
			}
		}
	}
}
