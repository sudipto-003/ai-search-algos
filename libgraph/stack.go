package libgraph

import "fmt"

type Stack struct {
	items []*GraphNode
	sp    int
}

func (s *Stack) New() *Stack {
	s.items = []*GraphNode{}
	s.sp = -1
	return s
}

func (s *Stack) Pop() *GraphNode {
	if s.sp != -1 {
		item := s.items[s.sp]
		s.items = s.items[:len(s.items)-1]
		s.sp--
		return item
	} else {
		return nil
	}
}

func (s *Stack) Push(node *GraphNode) {
	s.sp++
	s.items = append(s.items, node)
}

func (s *Stack) IsEmpty() bool {
	return len(s.items) == 0
}

func (s *Stack) Size() int {
	return len(s.items)
}

func (s *Stack) String() {
	for i := 0; i < len(s.items); i++ {
		fmt.Printf("%v\t", s.items[i])
	}
	fmt.Printf("\n")
}
