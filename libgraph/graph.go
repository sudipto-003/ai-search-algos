package libgraph

import "strconv"

type GraphNode struct {
	value int
}

func NewNode(value int) *GraphNode {
	return &GraphNode{value}
}

func (n *GraphNode) String() string {
	return strconv.Itoa(n.value)
}

type Graph struct {
	nodes []*GraphNode
	edges map[GraphNode][]*GraphNode
}

func (g *Graph) AddNode(n *GraphNode) {
	g.nodes = append(g.nodes, n)
}

func (g *Graph) AddEdge(n1, n2 *GraphNode) {
	if g.edges == nil {
		g.edges = make(map[GraphNode][]*GraphNode)
	}

	g.edges[*n1] = append(g.edges[*n1], n2)
	g.edges[*n2] = append(g.edges[*n2], n1)
}

func (g *Graph) Neighbours(node GraphNode) []*GraphNode {
	return g.edges[node]
}

func (g *Graph) GetNode(idx int) *GraphNode {
	return g.nodes[idx]
}

func (g *Graph) String() string {
	s := ""
	for i := 0; i < len(g.nodes); i++ {
		s += g.nodes[i].String() + " -> "
		neighbours := g.edges[*g.nodes[i]]
		for j := 0; j < len(neighbours); j++ {
			s += neighbours[j].String() + " "
		}
		s += "\n"
	}

	return s
}
