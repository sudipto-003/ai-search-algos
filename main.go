package main

import (
	"fmt"

	"gitlab.com/sudipto-003/ai-search-algos/libgraph"
)

var g libgraph.Graph

func fillGraph() {
	n0 := libgraph.NewNode(0)
	n1 := libgraph.NewNode(1)
	n2 := libgraph.NewNode(2)
	n3 := libgraph.NewNode(3)
	n4 := libgraph.NewNode(4)
	n5 := libgraph.NewNode(5)

	g.AddNode(n0)
	g.AddNode(n1)
	g.AddNode(n2)
	g.AddNode(n3)
	g.AddNode(n4)
	g.AddNode(n5)

	g.AddEdge(n0, n1)
	g.AddEdge(n0, n2)
	g.AddEdge(n0, n3)
	g.AddEdge(n1, n4)
	g.AddEdge(n2, n5)
	g.AddEdge(n3, n5)

}

func main() {
	fillGraph()
	final := "5"
	g.DFS(0, func(n *libgraph.GraphNode) int {
		if n.String() == final {
			fmt.Printf("State %v ==> Goal Reached\n", n)
			return -1
		} else {
			fmt.Printf("State %v ==> Not Goal State\n", n)
			return 0
		}
	})
}
